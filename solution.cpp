#include <bits/stdc++.h>

using namespace std;

class Solution
{
public:
    // 并查集的结构
    vector<int> un;
    // 合并父节点的find
    int unionfind(int x)
    {
        if (x != un[x])
            un[x] = unionfind(un[x]);
        return un[x];
    }
    vector<bool> friendRequests(int n, vector<vector<int>> &rest, vector<vector<int>> &req)
    {
        for (int i = 0; i < n; i++){
            // 初始化状态
            un.push_back(i);
        }
        // 合并前的并查集状态备份
        vector<int> originUnion;
        // result
        vector<bool> res;
        for (const auto &nowUnion : req)
        {
            int x = nowUnion[0], y = nowUnion[1];
            originUnion = un;
            int fx = unionfind(x), fy = unionfind(y);
            if (fx == fy)
            {
                // 在同一个并查集中
                res.push_back(true);
                continue;
            }
            un[fx] = fy;
            // 是否合法
            bool valid = true;
            for (const auto &enemies : rest)
            {
                int u = enemies[0], v = enemies[1];
                if (unionfind(u) == unionfind(v))
                {
                    valid = false;
                    break;
                }
            }
            // 有仇人
            if (!valid)
            {
                un = originUnion;
                res.push_back(false);
            }
            // 可以合并
            else
            {
                res.push_back(true);
            }
        }
        return res;
    }
};

int main(int argc, const char **argv)
{
    Solution so;
    int n = 3;
    vector<vector<int>> rest;
    vector<int> nowRest = {0, 1};
    rest.push_back(nowRest);

    vector<vector<int>> req;
    vector<int> nowReq1 = {0, 2};
    req.push_back(nowReq1);
    vector<int> nowReq2 = {1, 2};
    req.push_back(nowReq2);

    vector<bool> res = so.friendRequests(n, rest, req);
    for(auto const& i : res){
        cout << i << '\t';
    }
    cout << endl;
    return 0;
}
