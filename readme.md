# 处理含限制条件的好友请求

## 仓库地址

[仓库地址](https://gitlab.com/yangyf83/friendRequests)

`https://gitlab.com/yangyf83/friendRequests`

## 题目简述

给你一个整数 `n` ，表示网络上的用户数目。每个用户按从 `0` 到 `n - 1` 进行编号。

给你一个下标从 0 开始的二维整数数组 `restrictions` ，其中 `restrictions[i] = [xi, yi]` 意味着用户 `xi` 和用户 `yi` 不能 成为 朋友 ，不管是 **直接** 还是通过其他用户 **间接** 。

最初，用户里没有人是其他用户的朋友。给你一个下标从 `0` 开始的二维整数数组 `requests` 表示好友请求的列表，其中 `requests[j] = [uj, vj]` 是用户 `uj` 和用户 `vj` 之间的一条好友请求。

如果 `uj` 和 `vj` 可以成为 朋友 ，那么好友请求将会 成功 。每个好友请求都会按列表中给出的顺序进行处理（即，`requests[j]` 会在 `requests[j + 1]` 前）。一旦请求成功，那么对所有未来的好友请求而言， `uj` 和 `vj` 将会 *成为直接朋友* 。

返回一个 布尔数组 `result` ，其中元素遵循此规则：如果第 `j` 个好友请求 **成功** ，那么 `result[j]` 就是 `true` ；否则，为 `false` 。

注意：如果 `uj` 和 `vj` 已经是直接朋友，那么他们之间的请求将仍然 **成功** 。

来源：力扣（LeetCode）

链接：[链接](https://leetcode.cn/problems/process-restricted-friend-requests)

著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

## 主要思路

这个题目应该是leetcode去年某次周赛的题目，做过仍然有印象。

本题是一个并查集的板子题，核心就是给每一个有联系的*朋友圈*建立一个并查集的集合，这个集合共同维护一个名单，即就是大家的*仇人*的并集。

在初始状态下，一共有 `n` 个并查集，每个并查集的元素都是它本身。在收到好友请求时，我们会去遍历对方所在的那个并查集中的所有元素，如果对方所在的那个并查集中并不包含自己的*仇人*或者两个元素在同一个并查集中，那么就合并两个并查集并返回true，如果包含，那就返回true。

## 代码

```cpp
class Solution
{
public:
    // 并查集的结构
    vector<int> un;
    // 合并父节点的find
    int unionfind(int x)
    {
        if (x != un[x])
            un[x] = unionfind(un[x]);
        return un[x];
    }
    vector<bool> friendRequests(int n, vector<vector<int>> &rest, vector<vector<int>> &req)
    {
        for (int i = 0; i < n; i++){
            // 初始化状态
            un.push_back(i);
        }
        // 合并前的并查集状态备份
        vector<int> originUnion;
        // result
        vector<bool> res;
        for (const auto &nowUnion : req)
        {
            int x = nowUnion[0], y = nowUnion[1];
            originUnion = un;
            int fx = unionfind(x), fy = unionfind(y);
            if (fx == fy)
            {
                // 在同一个并查集中
                res.push_back(true);
                continue;
            }
            un[fx] = fy;
            // 是否合法
            bool valid = true;
            for (const auto &enemies : rest)
            {
                int u = enemies[0], v = enemies[1];
                if (unionfind(u) == unionfind(v))
                {
                    valid = false;
                    break;
                }
            }
            // 有仇人
            if (!valid)
            {
                // 回退到合并之前
                un = originUnion;
                res.emplace_back(false);
            }
            // 可以合并
            else
            {
                res.emplace_back(true);
            }
        }
        return res;
    }
};
```
